EESchema Schematic File Version 2  date Пн 25 ноя 2013 17:05:31
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:phinter
LIBS:project-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "25 nov 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1200 3100 0    60   ~ 0
PHOTO INTERRUPTER\n\nProduct Name	 Slotted Optical Switch\nModel	 HY860C\nOutput Type	 Photo Transistor\nPeak Emission Wavelength	 940nm\nIR Diode Forward Current	 50mA\nIR Diode Reverse Voltage	 5V\nTransistor Power Dissipation	 75mW\nTransistor Collector Current	 20mA\nPhoto Transistor Collector-emitter Voltage	 30V\nPhoto Transistor Emitter-collector Voltage	 5V\nDiode Power Dissipation	 100mW\nOperating Temperature Range	 -55~100 Celsius Degree\nNumber of Pins	 4\nSlot Width	 3.2mm/1/8''\nSlot Depth	 8mm/5/16''\nPin Length	 9mm/0.35''\nPitch	 7mm/0.28'', 2.5mm/0.1''\nHole Dia.	 3mm/0.12''\nTotal Size	 25 x 23 x 6mm/1'' x 0.9'' x 0.2''(L*W*H)\nMaterial	 Plastic, Metal\nColor	 Black\nNet Weight	 9g
Wire Wire Line
	5900 3400 5900 3500
Wire Wire Line
	5900 3900 5900 4100
Wire Wire Line
	5900 4500 5900 4600
Wire Wire Line
	4300 3900 4300 4000
Wire Wire Line
	6200 2800 6200 3900
Wire Wire Line
	6200 3900 6300 3900
Wire Wire Line
	5500 4300 5600 4300
Wire Wire Line
	5600 4300 5600 5200
Wire Wire Line
	6200 5200 6200 4100
Wire Wire Line
	6200 4100 6300 4100
Wire Wire Line
	4400 4300 4300 4300
Wire Wire Line
	4300 4300 4300 5200
Wire Wire Line
	5600 2800 5600 3400
Wire Wire Line
	4300 4000 4400 4000
Wire Wire Line
	4300 3400 4300 2800
Wire Wire Line
	5600 4000 5600 3900
Connection ~ 5600 4000
Connection ~ 5900 4000
Wire Wire Line
	6300 4000 5500 4000
Wire Wire Line
	5900 5100 5900 5200
Wire Wire Line
	5900 2800 5900 2900
$Comp
L VCC #PWR01
U 1 1 5292FF2B
P 5900 2800
F 0 "#PWR01" H 5900 2900 30  0001 C CNN
F 1 "VCC" H 5900 2900 30  0000 C CNN
	1    5900 2800
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5292FF21
P 5900 3150
F 0 "R3" V 5980 3150 50  0000 C CNN
F 1 "4K7" V 5900 3150 50  0000 C CNN
	1    5900 3150
	-1   0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 5292FED6
P 5900 3700
F 0 "D1" H 5900 3800 50  0000 C CNN
F 1 "LED" H 5900 3600 50  0000 C CNN
	1    5900 3700
	0    -1   1    0   
$EndComp
Text Label 6100 4000 0    60   ~ 0
SIG
$Comp
L GND #PWR02
U 1 1 5292FE5E
P 5900 5200
F 0 "#PWR02" H 5900 5200 30  0001 C CNN
F 1 "GND" H 5900 5130 30  0001 C CNN
	1    5900 5200
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5292FDAC
P 5900 4850
F 0 "R4" V 5980 4850 50  0000 C CNN
F 1 "2K2" V 5900 4850 50  0000 C CNN
	1    5900 4850
	-1   0    0    -1  
$EndComp
$Comp
L LED D2
U 1 1 5292FD92
P 5900 4300
F 0 "D2" H 5900 4400 50  0000 C CNN
F 1 "LED" H 5900 4200 50  0000 C CNN
	1    5900 4300
	0    -1   1    0   
$EndComp
$Comp
L CONN_3 K1
U 1 1 5292FCD2
P 6650 4000
F 0 "K1" V 6600 4000 50  0000 C CNN
F 1 "TO_MCU" V 6700 4000 40  0000 C CNN
	1    6650 4000
	1    0    0    1   
$EndComp
$Comp
L PHINTER U1
U 1 1 525A84B8
P 4950 4150
F 0 "U1" H 4950 4475 70  0000 C CNN
F 1 "PHINTER" H 4950 3800 70  0000 C CNN
	1    4950 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 525A84B6
P 6200 5200
F 0 "#PWR03" H 6200 5200 30  0001 C CNN
F 1 "GND" H 6200 5130 30  0001 C CNN
	1    6200 5200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 525A84B3
P 4300 5200
F 0 "#PWR04" H 4300 5200 30  0001 C CNN
F 1 "GND" H 4300 5130 30  0001 C CNN
	1    4300 5200
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 525A84B2
P 4300 3650
F 0 "R1" V 4380 3650 50  0000 C CNN
F 1 "220R" V 4300 3650 50  0000 C CNN
	1    4300 3650
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR05
U 1 1 525A84B1
P 4300 2800
F 0 "#PWR05" H 4300 2900 30  0001 C CNN
F 1 "VCC" H 4300 2900 30  0000 C CNN
	1    4300 2800
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 525A84B0
P 5600 3650
F 0 "R2" V 5680 3650 50  0000 C CNN
F 1 "1K" V 5600 3650 50  0000 C CNN
	1    5600 3650
	-1   0    0    -1  
$EndComp
$Comp
L VCC #PWR06
U 1 1 525A84AF
P 5600 2800
F 0 "#PWR06" H 5600 2900 30  0001 C CNN
F 1 "VCC" H 5600 2900 30  0000 C CNN
	1    5600 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 525A84AE
P 5600 5200
F 0 "#PWR07" H 5600 5200 30  0001 C CNN
F 1 "GND" H 5600 5130 30  0001 C CNN
	1    5600 5200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR08
U 1 1 525A84AD
P 6200 2800
F 0 "#PWR08" H 6200 2900 30  0001 C CNN
F 1 "VCC" H 6200 2900 30  0000 C CNN
	1    6200 2800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
