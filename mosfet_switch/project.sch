EESchema Schematic File Version 2  date Чт 19 дек 2013 10:03:31
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:project-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "19 dec 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6400 3000 6400 3300
Wire Wire Line
	7000 3900 7000 3700
Wire Wire Line
	7000 3700 7100 3700
Wire Wire Line
	7100 3100 7000 3100
Wire Wire Line
	4400 3500 4400 4100
Wire Wire Line
	4400 4100 4300 4100
Wire Wire Line
	4400 4900 4400 4300
Wire Wire Line
	4400 4300 4300 4300
Wire Wire Line
	6400 2500 6400 2600
Wire Wire Line
	6800 3400 6800 3300
Wire Wire Line
	6800 3800 6800 3900
Wire Wire Line
	5000 4800 5000 4900
Connection ~ 5000 4200
Connection ~ 5800 4500
Wire Wire Line
	5800 4100 5800 4900
Wire Wire Line
	5400 2700 5400 2600
Wire Wire Line
	5400 2600 5800 2600
Wire Wire Line
	6300 4700 6300 4900
Wire Wire Line
	5400 4400 5400 4500
Wire Wire Line
	5400 4500 5800 4500
Connection ~ 5400 3300
Wire Wire Line
	5400 3900 5500 3900
Connection ~ 5800 3600
Wire Wire Line
	5900 3600 5800 3600
Wire Wire Line
	5800 3700 5800 3500
Wire Wire Line
	5500 3300 5400 3300
Wire Wire Line
	5400 3200 5400 4000
Connection ~ 5400 3900
Wire Wire Line
	6300 4100 6300 4300
Wire Wire Line
	5800 2500 5800 3100
Connection ~ 5800 2600
Wire Wire Line
	4900 4200 5100 4200
Wire Wire Line
	5000 4100 5000 4300
Wire Wire Line
	5000 3500 5000 3600
Wire Wire Line
	6500 3600 6400 3600
Wire Wire Line
	4300 4200 4400 4200
Connection ~ 6800 3300
Wire Wire Line
	6400 3300 7100 3300
Wire Wire Line
	7000 2500 7000 3500
Wire Wire Line
	7000 3500 7100 3500
Connection ~ 7000 3100
$Comp
L CONN_2 P2
U 1 1 52B26D04
P 7450 3200
F 0 "P2" V 7400 3200 40  0000 C CNN
F 1 "LOAD" V 7500 3200 40  0000 C CNN
	1    7450 3200
	1    0    0    1   
$EndComp
$Comp
L CONN_2 P3
U 1 1 52B26CE6
P 7450 3600
F 0 "P3" V 7400 3600 40  0000 C CNN
F 1 "PWR" V 7500 3600 40  0000 C CNN
	1    7450 3600
	1    0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 52B1C499
P 7000 3900
F 0 "#PWR01" H 7000 3900 30  0001 C CNN
F 1 "GND" H 7000 3830 30  0001 C CNN
	1    7000 3900
	1    0    0    -1  
$EndComp
Text Label 5800 3600 0    60   ~ 0
G
Text Label 4300 4200 0    60   ~ 0
LIN
Text Label 5000 4200 0    60   ~ 0
CTL
Text Label 5400 3650 0    60   ~ 0
DRV
Text Label 6800 3300 0    60   ~ 0
PSW
$Comp
L VDD #PWR02
U 1 1 52B1BC3A
P 4400 3500
F 0 "#PWR02" H 4400 3600 30  0001 C CNN
F 1 "VDD" H 4400 3610 30  0000 C CNN
	1    4400 3500
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 P1
U 1 1 52B1BC14
P 3950 4200
F 0 "P1" V 3900 4200 50  0000 C CNN
F 1 "^CTL" V 4000 4200 40  0000 C CNN
	1    3950 4200
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR03
U 1 1 52B1B504
P 4400 4900
F 0 "#PWR03" H 4400 4900 30  0001 C CNN
F 1 "GND" H 4400 4830 30  0001 C CNN
	1    4400 4900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 52B1B35F
P 6400 2500
F 0 "#PWR04" H 6400 2600 30  0001 C CNN
F 1 "VCC" H 6400 2600 30  0000 C CNN
	1    6400 2500
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR05
U 1 1 52B1B34B
P 7000 2500
F 0 "#PWR05" H 7000 2600 30  0001 C CNN
F 1 "VCC" H 7000 2600 30  0000 C CNN
	1    7000 2500
	1    0    0    -1  
$EndComp
$Comp
L DIODESCH D1
U 1 1 52B1B30B
P 6400 2800
F 0 "D1" H 6400 2900 40  0000 C CNN
F 1 "DIODESCH" H 6400 2700 40  0000 C CNN
F 4 "Inductivity load only" H 6400 2600 60  0000 C CNN "Note"
	1    6400 2800
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR06
U 1 1 52B1B2EE
P 6800 3900
F 0 "#PWR06" H 6800 3900 30  0001 C CNN
F 1 "GND" H 6800 3830 30  0001 C CNN
	1    6800 3900
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q4
U 1 1 52B1B2DC
P 6700 3600
F 0 "Q4" H 6710 3770 60  0000 R CNN
F 1 "B55NF03" H 6710 3450 60  0000 R CNN
	1    6700 3600
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR07
U 1 1 52B1B1D0
P 5000 3500
F 0 "#PWR07" H 5000 3600 30  0001 C CNN
F 1 "VDD" H 5000 3610 30  0000 C CNN
	1    5000 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 52B1B16D
P 6300 4900
F 0 "#PWR08" H 6300 4900 30  0001 C CNN
F 1 "GND" H 6300 4830 30  0001 C CNN
	1    6300 4900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR09
U 1 1 52B1B169
P 6300 4100
F 0 "#PWR09" H 6300 4200 30  0001 C CNN
F 1 "VCC" H 6300 4200 30  0000 C CNN
	1    6300 4100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR010
U 1 1 52B1B163
P 5800 2500
F 0 "#PWR010" H 5800 2600 30  0001 C CNN
F 1 "VCC" H 5800 2600 30  0000 C CNN
	1    5800 2500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 52B1B15C
P 5000 4900
F 0 "#PWR011" H 5000 4900 30  0001 C CNN
F 1 "GND" H 5000 4830 30  0001 C CNN
	1    5000 4900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 52B1B158
P 5800 4900
F 0 "#PWR012" H 5800 4900 30  0001 C CNN
F 1 "GND" H 5800 4830 30  0001 C CNN
	1    5800 4900
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 52B1B12B
P 6300 4500
F 0 "C1" H 6350 4600 50  0000 L CNN
F 1 "10n" H 6350 4400 50  0000 L CNN
	1    6300 4500
	-1   0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 52B1B119
P 5000 3850
F 0 "R2" V 5080 3850 50  0000 C CNN
F 1 "10K" V 5000 3850 50  0000 C CNN
F 4 "Default is close" V 4900 3750 60  0000 C CNN "Note"
	1    5000 3850
	-1   0    0    1   
$EndComp
$Comp
L R R3
U 1 1 52B1B111
P 5000 4550
F 0 "R3" V 5080 4550 50  0000 C CNN
F 1 "10K" V 5000 4550 50  0000 C CNN
F 4 "Default is open" V 4900 4600 60  0000 C CNN "Note"
	1    5000 4550
	-1   0    0    1   
$EndComp
$Comp
L R R1
U 1 1 52B1B0F8
P 4650 4200
F 0 "R1" V 4730 4200 50  0000 C CNN
F 1 "1K" V 4650 4200 50  0000 C CNN
	1    4650 4200
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 52B1B0EC
P 5400 2950
F 0 "R4" V 5480 2950 50  0000 C CNN
F 1 "10K" V 5400 2950 50  0000 C CNN
	1    5400 2950
	-1   0    0    1   
$EndComp
$Comp
L R R5
U 1 1 52B1B0E0
P 6150 3600
F 0 "R5" V 6230 3600 50  0000 C CNN
F 1 "100" V 6150 3600 50  0000 C CNN
	1    6150 3600
	0    -1   -1   0   
$EndComp
$Comp
L NPN Q1
U 1 1 52B1B0C3
P 5300 4200
F 0 "Q1" H 5300 4350 50  0000 R CNN
F 1 "2N3904" H 5750 4100 50  0000 R CNN
	1    5300 4200
	1    0    0    -1  
$EndComp
$Comp
L PNP Q3
U 1 1 52B1B0B8
P 5700 3900
F 0 "Q3" H 5700 3750 60  0000 R CNN
F 1 "2N3906" H 6200 4000 60  0000 R CNN
	1    5700 3900
	1    0    0    1   
$EndComp
$Comp
L NPN Q2
U 1 1 52B1B0A9
P 5700 3300
F 0 "Q2" H 5700 3450 50  0000 R CNN
F 1 "2N3904" H 6150 3200 50  0000 R CNN
	1    5700 3300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
