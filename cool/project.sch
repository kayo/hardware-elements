EESchema Schematic File Version 2  date Пт 13 дек 2013 14:51:10
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:project-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "13 dec 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 5750 4100
Wire Wire Line
	5550 4100 5550 4000
Wire Wire Line
	5550 4100 6350 4100
Connection ~ 5750 2800
Wire Wire Line
	5450 3100 5350 3100
Wire Wire Line
	5350 3100 5350 2800
Wire Wire Line
	5350 2800 6350 2800
Connection ~ 6350 2900
Wire Wire Line
	6450 2900 6350 2900
Wire Wire Line
	6150 3200 6150 3100
Wire Wire Line
	6150 3100 6450 3100
Connection ~ 6150 4100
Wire Wire Line
	6150 4200 6150 3600
Wire Wire Line
	6450 3600 6350 3600
Wire Wire Line
	5750 3300 5750 3500
Connection ~ 5750 3400
Wire Wire Line
	5750 4100 5750 4000
Wire Wire Line
	6350 3600 6350 4100
Wire Wire Line
	6450 3400 6350 3400
Wire Wire Line
	6350 3400 6350 2800
Wire Wire Line
	5750 2800 5750 2900
Wire Wire Line
	6150 2700 6150 2800
Connection ~ 6150 2800
Wire Wire Line
	5850 3400 5550 3400
Wire Wire Line
	5550 3400 5550 3500
$Comp
L R R2
U 1 1 52AACA6B
P 5750 3750
F 0 "R2" V 5830 3750 50  0000 C CNN
F 1 "100K" V 5750 3750 50  0000 C CNN
	1    5750 3750
	-1   0    0    -1  
$EndComp
$Comp
L +12V #PWR01
U 1 1 5190D569
P 6150 2700
F 0 "#PWR01" H 6150 2650 20  0001 C CNN
F 1 "+12V" H 6150 2800 30  0000 C CNN
	1    6150 2700
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P1
U 1 1 5190D505
P 6800 3000
F 0 "P1" V 6750 3000 40  0000 C CNN
F 1 "DRV" V 6850 3000 40  0000 C CNN
	1    6800 3000
	1    0    0    1   
$EndComp
$Comp
L CONN_2 P2
U 1 1 5190D501
P 6800 3500
F 0 "P2" V 6750 3500 40  0000 C CNN
F 1 "PWR" V 6850 3500 40  0000 C CNN
	1    6800 3500
	1    0    0    1   
$EndComp
$Comp
L GND #PWR02
U 1 1 5190D436
P 6150 4200
F 0 "#PWR02" H 6150 4200 30  0001 C CNN
F 1 "GND" H 6150 4130 30  0001 C CNN
	1    6150 4200
	1    0    0    -1  
$EndComp
$Comp
L MOS_N Q2
U 1 1 5190D3D0
P 6050 3400
F 0 "Q2" H 6060 3570 60  0000 R CNN
F 1 "14N03L" V 6060 3250 60  0000 R CNN
	1    6050 3400
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5190D3A6
P 5550 3750
F 0 "R1" V 5630 3750 50  0000 C CNN
F 1 "100K" V 5550 3750 50  0000 C CNN
	1    5550 3750
	-1   0    0    -1  
$EndComp
$Comp
L PNP Q1
U 1 1 5190D39E
P 5650 3100
F 0 "Q1" H 5650 2950 60  0000 R CNN
F 1 "МП40" H 5650 3250 60  0000 R CNN
	1    5650 3100
	1    0    0    1   
$EndComp
$EndSCHEMATC
