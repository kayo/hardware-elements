EESchema Schematic File Version 2  date Вск 28 Апр 2013 20:50:35
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:project-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "28 apr 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4800 3100 4800 3700
Wire Wire Line
	4800 3700 5400 3700
Wire Wire Line
	5000 3100 5000 3500
Wire Wire Line
	5000 3500 5400 3500
Wire Wire Line
	5200 3100 5200 3300
Wire Wire Line
	5200 3300 5400 3300
Wire Wire Line
	5400 3200 5300 3200
Wire Wire Line
	5300 3200 5300 3100
Wire Wire Line
	5400 3400 5100 3400
Wire Wire Line
	5100 3400 5100 3100
Wire Wire Line
	5400 3600 4900 3600
Wire Wire Line
	4900 3600 4900 3100
$Comp
L CONN_1 P6
U 1 1 517D370E
P 5550 3700
F 0 "P6" H 5630 3700 40  0000 L CNN
F 1 "CONN_1" H 5550 3755 30  0001 C CNN
	1    5550 3700
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P5
U 1 1 517D370C
P 5550 3600
F 0 "P5" H 5630 3600 40  0000 L CNN
F 1 "CONN_1" H 5550 3655 30  0001 C CNN
	1    5550 3600
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P4
U 1 1 517D3708
P 5550 3500
F 0 "P4" H 5630 3500 40  0000 L CNN
F 1 "CONN_1" H 5550 3555 30  0001 C CNN
	1    5550 3500
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P3
U 1 1 517D3706
P 5550 3400
F 0 "P3" H 5630 3400 40  0000 L CNN
F 1 "CONN_1" H 5550 3455 30  0001 C CNN
	1    5550 3400
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P2
U 1 1 517D3701
P 5550 3300
F 0 "P2" H 5630 3300 40  0000 L CNN
F 1 "CONN_1" H 5550 3355 30  0001 C CNN
	1    5550 3300
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P1
U 1 1 517D36F2
P 5550 3200
F 0 "P1" H 5630 3200 40  0000 L CNN
F 1 "CONN_1" H 5550 3255 30  0001 C CNN
	1    5550 3200
	1    0    0    -1  
$EndComp
$Comp
L RJ12 J1
U 1 1 517D36B3
P 5100 2650
F 0 "J1" H 5300 3150 60  0000 C CNN
F 1 "RJ12" H 4950 3150 60  0000 C CNN
	1    5100 2650
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
